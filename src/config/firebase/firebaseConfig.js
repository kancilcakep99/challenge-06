// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import {getAuth} from '@firebase/auth';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDi3iYeUK3zEA2a8LfWLvEi4tFzrZQuzes",
  authDomain: "challenge-06-a65c2.firebaseapp.com",
  projectId: "challenge-06-a65c2",
  storageBucket: "challenge-06-a65c2.appspot.com",
  messagingSenderId: "1048665768929",
  appId: "1:1048665768929:web:1503138181493d7cbec8a0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const authentication = getAuth(app);