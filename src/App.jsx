import Register from './components/Register';
import Login from './components/Login';
import Dashboard from './components/Home/Dashboard';
import LandingCar from './components/Landingcar/LandingCar';
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';


function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route path="/" element={<Register/>} />
          <Route path="/login" element={<Login/>} />
          <Route path="/dashboard" element= {<Dashboard/>} />
          <Route path="/landingcar" element={<LandingCar/>} />
        </Routes>
      </div>
    </Router> 
  );
}

export default App;
