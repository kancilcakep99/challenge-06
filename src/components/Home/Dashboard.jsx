import React, { useState } from 'react'
import {
    FiHome,
    FiTruck,
    FiSearch,
} from "react-icons/fi";
import { Navbar, Container, Form, Button, Row, Col, FormControl } from 'react-bootstrap';
import { FaBars } from "react-icons/fa";
import { useNavigate, NavLink } from 'react-router-dom';
import { useEffect } from "react";
import { useSelector } from "react-redux";

import '../../style/dashboard.css';

const Dashboard = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen); 

    const dataLogin = useSelector((state) => state.auth);

    const navigate = useNavigate();

    useEffect(() => {
        if(dataLogin?.email !== 'admin@admin.com') navigate("/")

        // eslint-disable-next-line
    }, [])


    const menuItem = [
        {
            path: "/Home",
            name: "Dashboard",
            icon: <FiHome />
        },
        {
            path: "/Cars",
            name: "Cars",
            icon: <FiTruck />
        },
    ]

    return (
        <>
            <Container>
                <Row>
                    <Col md="6">
                        <Navbar className="hemo">
                            <Container>
                                <Navbar.Brand href="#" className="tape"></Navbar.Brand>
                                <Navbar.Brand href="#" className="tapes"> <FaBars /> </Navbar.Brand>
                                <Navbar.Toggle aria-controls="navbarScroll" />
                                <Navbar.Collapse id="navbarScroll" className="justif">
                                    <Form>
                                        <FiSearch className="search" />
                                        <FormControl
                                            type="search"
                                            placeholder="Search"
                                            className="dex"
                                            aria-label="Search"
                                        ></FormControl>
                                        <Button variant="outline-primary" className="buttonn">Search</Button>
                                    </Form>
                                </Navbar.Collapse>
                            </Container>
                        </Navbar>
                    </Col>

                    <Col md="6">
                        <div style={{ width: isOpen ? "200px" : "50px" }} className="sidebar">
                            <div className="top_section">
                                <img src="./Rectangle.png" alt="" style={{ display: isOpen ? "block" : "none" }} />
                                <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
                                    <FaBars onClick={toggle} />
                                </div>
                            </div>
                            {
                                menuItem.map((item, index) => (
                                    <NavLink to={item.path} key={index} className="links" >
                                        <div className="icon">{item.icon}</div>
                                        <div style={{ display: isOpen ? "block" : "none" }} className="links_text">{item.name}</div>
                                    </NavLink> 
                                ))
                            }
                            {menuItem.path}
                            
                        </div>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Dashboard