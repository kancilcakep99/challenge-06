import React from 'react';
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import '../../style/landingcar.css';
import {Navbar,Container,Nav,Button,Row,Col} from 'react-bootstrap';


const LandingCar = () => {
    const data = useSelector((globalStore) => globalStore);

    const navigate = useNavigate(); 

    const CheckLogin = () => { 
        if (
            data.auth.dataLogin === null ||
            data.auth.dataLogin?.email === "admin@admin.com"
          )
            navigate("/");
    };

    useEffect(() => {
        CheckLogin();
        // eslint-disable-next-line
    }, [])

  return (
    <>
        <Navbar className="hero">
            <Container>
                <Navbar.Brand href="#" className="tap">
                    <img src="./logo.png" alt="" />
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify">
                <Nav>
                    <Nav.Link href="#action1" className="link">Our Services</Nav.Link>
                    <Nav.Link href="#action2" className="link">Why Us</Nav.Link>
                    <Nav.Link href="#action3" className="link">Testimonial</Nav.Link>
                    <Nav.Link href="#action4" className="link">FAQ</Nav.Link>
                    <Button type="button" className="button">Register</Button>
                    
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

        <div className="sections">
            <Container>
                <Row>
                    <Col md="6">
                        <h1 className="title">Sewa & Rental Mobil Terbaik di<br/> kawasan (Lokasimu)</h1>
                        <p className="tex">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas<br/> terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu<br/> untuk sewa mobil selama 24 jam.</p>
                        <Button variant="success" className="buton">Mulai Sewa Mobil</Button>{' '}
                    </Col>

                    <Col md="6">
                        <img src="./img_car.png" alt="" className="image" /> 
                    </Col>
                </Row>
            </Container>
        </div>
    </>
  )
}

export default LandingCar